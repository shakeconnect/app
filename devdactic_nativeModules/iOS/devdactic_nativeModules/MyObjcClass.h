#import "RCTBridgeModule.h"
#import "RCTEventDispatcher.h"
#import "RFduino.h"
#import "RFduinoManager.h"
#import "RFduinoDelegate.h"
#import "RFduinoManagerDelegate.h"

@interface MyObjcClass : NSObject <RFduinoDelegate, RCTBridgeModule>

@property(strong, nonatomic) RFduino *rfduino;

@end