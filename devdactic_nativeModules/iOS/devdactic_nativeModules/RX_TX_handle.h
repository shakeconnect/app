//
//  RX_TX_handle.h
//  devdactic_nativeModules
//
//  Created by Sean Rowan on 12/09/2015.
//  Copyright © 2015 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RFduino.h"

@interface RX_TX_handle : NSObject<RFduinoDelegate>

@property(strong, nonatomic) RFduino *rfduino;

@end
