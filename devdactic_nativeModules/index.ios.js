/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var REQUEST_URL = 'http://46.101.32.152/data/index.php';

var React = require('react-native');

var body_string = "app_id=";

var { NativeAppEventEmitter } = require('react-native');

var 
{
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight
} = React;

var MyObjcClass = require('NativeModules').MyObjcClass;

var devdactic_nativeModules = React.createClass({
  getInitialState: function() {
    return {
      _num_presses: null,
    };
  },

  componentDidMount: function() {
    MyObjcClass.rfduinoInit((error) => {
      if (error) {
        console.error(error);
      }
      else {
        MyObjcClass.rfduinoConnect((error) => {
          if (error) {
            console.error(error);
          }
        })
      }
    })

    var subscription = NativeAppEventEmitter.addListener(
        'ServerCode',
        (_RX) => this.fetchData(_RX.code)
        //(_RX) => console.log(_RX.code)//,
        //this.fetchData();
    );
  },

  _handleRX(event) {
    console.log('RCT_RX');
    this.fetchData();
  },

  handleRX: function(server_code) {
    this.fetchData();
  },

  fetchData: function(server_code) {
    var send_string = body_string.concat(server_code);
    fetch(REQUEST_URL, 
    {  
      method: 'post',  
      headers: 
      {  
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"  
      },  
      body: send_string 
    })
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          _num_presses: responseData,
        });
      })
      .done();
  },

  render: function() 
  {
    if (!this.state._num_presses) 
    {
      return this.renderLoadingView();
    }

    var num_presses = this.state._num_presses;
    return this.renderPerson(num_presses);
  },

  renderLoadingView: function() {
    return (
      <View style={styles.container}>
        <Text>
          Loading number of button presses...
        </Text>
      </View>
    );
  },

  renderPerson: function(num_presses) {
    return (
      <View style={styles.container}>
          <Text style={styles.titleText}>{num_presses}</Text>
      </View>
    );
  },


});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  titleText: {
    fontSize: 120,
    fontWeight: 'bold',
  },
});

AppRegistry.registerComponent('devdactic_nativeModules', () => devdactic_nativeModules);
