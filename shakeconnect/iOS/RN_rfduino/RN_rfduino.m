#import "RN_rfduino.h"

@interface RN_rfduino()
{
  RFduinoManager *rfduinoManager;
  RFduino *rfduino;
  bool wasScanning;
}
@end

@implementation RN_rfduino

@synthesize rfduino;

@synthesize bridge = _bridge;

- (void)didReceive:(NSData *)data
{
  float server_code = dataFloat(data);
  NSString *eventString = [NSString stringWithFormat:@"%.0f", server_code];
  NSLog(@"RecievedRX %@", eventString);
  [self.bridge.eventDispatcher sendAppEventWithName:@"ServerCode"
                                               body:@{@"code": eventString}];
  
}

// The React Native bridge needs to know our module
RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(rfduinoInit:(RCTResponseSenderBlock)callback){
  callback(@[[NSNull null]]);
  rfduinoManager = RFduinoManager.sharedRFduinoManager;
}

RCT_EXPORT_METHOD(rfduinoConnect:(RCTResponseSenderBlock)callback){
  callback(@[[NSNull null]]);
  while([[rfduinoManager rfduinos] count] == 0) {}
  rfduino = [[rfduinoManager rfduinos] objectAtIndex: 0];
  [rfduinoManager connectRFduino:rfduino];
  [rfduino setDelegate:self];
}

RCT_EXPORT_METHOD(rfduinoRXreceived:(RCTResponseSenderBlock)callback){
  NSLog(@"rfduinoRXreceived");
  callback(@[[NSNull null], [NSNumber numberWithFloat:(0)]]);
}

@end






