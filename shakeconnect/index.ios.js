'use strict';

var REQUEST_URL = 'http://shakeconnectserver.ddns.net/data/index.php';

var React = require('react-native');

var body_string_0 = "band_id=";
var body_string_1 = "&unique_id=";
var body_string_2 = "&request_id=";

var { NativeAppEventEmitter } = require('react-native');

var 
{
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ListView,
  ScrollView,
} = React;

var RN_rfduino = require('NativeModules').RN_rfduino;

var shakeconnect = React.createClass({
  getInitialState: function() {
    return {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      loaded: false,
    };
  },

  componentDidMount: function() {
    this.setState({
          dataSource: this.state.dataSource.cloneWithRows([{"0":"0"}]),
    });

    RN_rfduino.rfduinoInit((error) => {
      if (error) {
        console.error(error);
      }
      else {
        RN_rfduino.rfduinoConnect((error) => {
          if (error) {
            console.error(error);
          }
        })
      }
    })

    var subscription = NativeAppEventEmitter.addListener(
        'ServerCode',
        (_RX) => this.send_unmatched_connection(_RX.code)
    );
  },

  make_random_id: function() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 16; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  },

  send_unmatched_connection: function(server_code) {
    var unique_id = this.make_random_id();
    var send_string = body_string_0.concat(server_code, body_string_1, unique_id);
    console.log(send_string);
    fetch(REQUEST_URL, 
    {  
      method: 'post',  
      headers: 
      {  
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"  
      },  
      body: send_string 
    })
    .then((response) => response.json())
    .then((responseData) => {
      setTimeout(()=>this.request_matched_connection(server_code), 2000);
    })
    .done();
    
  },

  request_matched_connection: function(server_code) {
    var request_id = this.make_random_id();
    var send_string = body_string_0.concat(server_code, body_string_2, request_id);
    console.log(send_string);
    fetch(REQUEST_URL, 
    {  
      method: 'post',  
      headers: 
      {  
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"  
      },  
      body: send_string 
    })
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(responseData),
          loaded: true,
        });
      })
      .done();
  },

  render: function() 
  {
    return (
      <View style={styles.main_background}>
        <View style={styles.header}>
        </View>
        <ListView
          automaticallyAdjustContentInsets={false}
          dataSource={this.state.dataSource}
          renderSectionHeader={this.renderNavBar}
          renderRow={this.renderPerson}
        />
      </View>
    );
  },

  renderLoadingView: function() {
    return (
      <View style={styles.container_header}>
        <Text>
          Waiting for new connections...
        </Text>
      </View>
    );
  },

  renderNavBar: function() {
    return (
      <View style={styles.container_header}>
          <Image
          source={{uri: 'http://shakeconnectserver.ddns.net/data/files/nav.png'}}
          style={styles.nav_stack}
          />

          <View style={styles.rightContainer}>
            <Text style={styles.nav_title}>new connections</Text>
          </View>
      </View>
    );
  },

  renderPerson: function(responseData) {
    if (!this.state.loaded) 
    {
      return this.renderLoadingView();
    }

    return (
      <View style={styles.container}>
          <Image
            source={{uri: responseData.their_image_url}}
            style={styles.thumbnail}
          />
          
          <View style={styles.rightContainer}>
            <Text style={styles.name}>{responseData.their_firstname} {responseData.their_lastname}</Text>
            <Text style={styles.title}>{responseData.their_title}</Text>
          </View>
      </View>
    );
  },


});

var styles = StyleSheet.create({
  main_background:{
    flex:1,
    backgroundColor:'#ffffff',
  },
  container_header:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#44a9ef',
  },
  //'#44a9ef''#cf8bd2''#ff7f7f''#cdcbc0'
  listView: {
    backgroundColor: '#FFFFFF',
  },
  rightContainer: {
    flex: 1,
  },
  nav_title: {
    fontFamily: 'Avenir Next',
    color: 'black',
    textAlign: 'center',
    fontSize: 25,
    marginTop: 8,
    marginBottom: 8,
  },
  name: {
    fontFamily: 'Avenir Next',
    color: 'white',
    fontSize: 20,
    marginLeft: 12,
    marginBottom: 0,
    textAlign: 'left',
  },
  title: {
    fontFamily: 'Avenir Next',
    color: 'white',
    textAlign: 'left',
    fontSize: 16,
    marginLeft: 12,
    marginBottom: 0,
  },
  thumbnail: {
    width: 85,
    height: 85,
  },
  nav_stack: {
    width: 20,
    height: 14,
    marginLeft: 15,
  },
  header: {
    height: 20,
    backgroundColor: '#ffffff'
  },
});

AppRegistry.registerComponent('shakeconnect', () => shakeconnect);
