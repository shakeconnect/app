//
//  ReactView.h
//  RN_IOS_TESTING
//
//  Created by Sean Rowan on 09/09/2015.
//  Copyright © 2015 Sean Rowan. All rights reserved.
//

#ifndef ReactView_h
#define ReactView_h

#import <UIKit/UIKit.h>
@interface ReactView : UIView
@end

#endif /* ReactView_h */
