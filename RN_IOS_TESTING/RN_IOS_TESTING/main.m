//
//  main.m
//  RN_IOS_TESTING
//
//  Created by Sean Rowan on 09/09/2015.
//  Copyright © 2015 Sean Rowan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
