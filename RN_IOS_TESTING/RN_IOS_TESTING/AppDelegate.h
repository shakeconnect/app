//
//  AppDelegate.h
//  RN_IOS_TESTING
//
//  Created by Sean Rowan on 09/09/2015.
//  Copyright © 2015 Sean Rowan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

