#import "ReactView.h"
#import "RCTRootView.h"

@implementation ReactView

- (void)awakeFromNib {
    NSString *urlString = @"http://192.168.0.13:8081/index.ios.bundle";
    NSURL *jsCodeLocation = [NSURL URLWithString:urlString];
    RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                        moduleName: @"RN_IOS_TESTING"
                                                     launchOptions:nil];
    [self addSubview:rootView];
    rootView.frame = self.bounds;
}

@end