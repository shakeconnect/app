/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var REQUEST_URL = 'http://46.101.32.152/data/index.php';

var React = require('react-native');


var {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight
} = React;


var NetworkingTest = React.createClass({
  getInitialState: function() {
    return {
      _num_presses: null,
    };
  },

  componentDidMount: function() {
    this.fetchData();
  },

  fetchData: function() {
    fetch(REQUEST_URL, 
    {  
      method: 'post',  
      headers: 
      {  
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"  
      },  
      body: 'app_id=1234'  
    })
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          _num_presses: responseData,
        });
      })
      .done();
  },

  render: function() {
    if (!this.state._num_presses) {
      return this.renderLoadingView();
    }

    var num_presses = this.state._num_presses;
    return this.renderPerson(num_presses);
  },

  renderLoadingView: function() {
    return (
      <View style={styles.container}>
        <Text>
          Loading number of button presses...
        </Text>
      </View>
    );
  },

  renderPerson: function(num_presses) {
    return (
      <View style={styles.container}>
        <TouchableHighlight onPress={this._handlePress} activeOpacity={255} underlayColor={'#F5FCFF'}>
          <Text style={styles.titleText}>{num_presses}</Text>
        </TouchableHighlight> 
      </View>
    );
  },

  _handlePress(event) {
    console.log('Pressed!');
    this.fetchData();
  },

});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  titleText: {
    fontSize: 150,
    fontWeight: 'bold',
  },
});

AppRegistry.registerComponent('NetworkingTest', () => NetworkingTest);
