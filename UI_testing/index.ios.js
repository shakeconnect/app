'use strict';

var REQUEST_URL = '/Users/sprowan/Documents/shakeconnect/app/Contacts.json';

var React = require('react-native');
var {
  AppRegistry,
  Image,
  ListView,
  StyleSheet,
  ScrollView,
  Text,
  View,
} = React;

var shakeconnect = React.createClass({
  getInitialState: function() {
    return {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      loaded: false,
    };
  },

  componentDidMount: function() {
    this.fetchData();
  },

  fetchData: function() {
    fetch(REQUEST_URL)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(responseData.people),
          loaded: true,
        });
      })
      .done();
  },

  render: function() {
    if (!this.state.loaded) {
      return this.renderLoadingView();
    }

    return (
      <View style={styles.main_background}>
        <View style={styles.header}>
        </View>
        <ListView
          automaticallyAdjustContentInsets={false}
          dataSource={this.state.dataSource}
          renderSectionHeader={this.renderNavBar}
          renderRow={this.renderPerson}
        />
      </View>
    );
  },

  renderLoadingView: function() {
    return (
      <View style={styles.container}>
        <Text>
          Loading contacts...
        </Text>
      </View>
    );
  },

  renderNavBar: function() {
    return (
      <View style={styles.container_header}>
          <Image
          source={{uri: '/Users/sprowan/Documents/shakeconnect/app/nav.png'}}
          style={styles.nav_stack}
          />

          <View style={styles.rightContainer}>
            <Text style={styles.nav_title}>new connections</Text>
          </View>
      </View>
    );
  },

  renderPerson: function(people) {
    return (
      <View style={styles.container}>
          <Image
            source={{uri: people.picture}}
            style={styles.thumbnail}
          />
          
          <View style={styles.rightContainer}>
            <Text style={styles.name}>{people.name}</Text>
            <Text style={styles.title}>{people.title}</Text>
          </View>
      </View>
    );
  },
});

var styles = StyleSheet.create({
  main_background:{
    flex:1,
    backgroundColor:'#ffffff',
  },
  container_header:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#44a9ef',
  },
  //'#44a9ef''#cf8bd2''#ff7f7f''#cdcbc0'
  listView: {
    backgroundColor: '#FFFFFF',
  },
  rightContainer: {
    flex: 1,
  },
  nav_title: {
    fontFamily: 'Avenir Next',
    color: 'black',
    textAlign: 'center',
    fontSize: 18,
    marginTop: 8,
    marginBottom: 8,
  },
  name: {
    fontFamily: 'Avenir Next',
    color: 'white',
    fontSize: 20,
    marginLeft: 12,
    marginBottom: 0,
    textAlign: 'left',
  },
  title: {
    fontFamily: 'Avenir Next',
    color: 'white',
    textAlign: 'left',
    fontSize: 16,
    marginLeft: 12,
    marginBottom: 0,
  },
  thumbnail: {
    width: 75,
    height: 75,
  },
  nav_stack: {
    width: 20,
    height: 14,
    marginLeft: 15,
  },
  header: {
    height: 20,
    backgroundColor: '#ffffff'
  },
});

AppRegistry.registerComponent('shakeconnect', () => shakeconnect);
