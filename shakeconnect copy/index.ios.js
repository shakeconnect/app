'use strict';

var REQUEST_URL = 'http://shakeconnectserver.ddns.net/data/index.php';

var React = require('react-native');

var body_string_0 = "band_id=";
var body_string_1 = "&unique_id=";
var body_string_2 = "&request_id=";

var { NativeAppEventEmitter } = require('react-native');

var 
{
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ListView,
  ScrollView,
} = React;

var RN_rfduino = require('NativeModules').RN_rfduino;

var shakeconnect = React.createClass({
  getInitialState: function() {
    return {
      _their_band_id: null,
    };
  },

  componentDidMount: function() {
    RN_rfduino.rfduinoInit((error) => {
      if (error) {
        console.error(error);
      }
      else {
        RN_rfduino.rfduinoConnect((error) => {
          if (error) {
            console.error(error);
          }
        })
      }
    })

    var subscription = NativeAppEventEmitter.addListener(
        'ServerCode',
        (_RX) => this.send_unmatched_connection(_RX.code)
    );
  },

  make_random_id: function() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 16; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  },

  send_unmatched_connection: function(server_code) {
    var unique_id = this.make_random_id();
    var send_string = body_string_0.concat(server_code, body_string_1, unique_id);
    console.log(send_string);
    fetch(REQUEST_URL, 
    {  
      method: 'post',  
      headers: 
      {  
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"  
      },  
      body: send_string 
    })
    .then((response) => response.json())
    .then((responseData) => {
      setTimeout(()=>this.request_matched_connection(server_code), 2000);
    })
    .done();
    
  },

  request_matched_connection: function(server_code) {
    var request_id = this.make_random_id();
    var send_string = body_string_0.concat(server_code, body_string_2, request_id);
    console.log(send_string);
    fetch(REQUEST_URL, 
    {  
      method: 'post',  
      headers: 
      {  
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"  
      },  
      body: send_string 
    })
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          _their_band_id: responseData,
        });
      })
      .done();
  },

  render: function() 
  {
    if (!this.state._their_band_id) 
    {
      return this.renderLoadingView();
    }

    var their_band_id = this.state._their_band_id;
    return this.renderPerson(their_band_id);
  },

  renderLoadingView: function() {
    return (
      <View style={styles.container}>
        <Text>
          Waiting for new connections...
        </Text>
      </View>
    );
  },

  renderPerson: function(their_band_id) {
    return (
      <View style={styles.container}>
          <Image
          source={{uri: their_band_id.their_image_url}}
          style={styles.nav_stack}
          />
          <Text>
            {their_band_id.their_firstname} {their_band_id.their_lastname}
          </Text>
          <Text>
            {their_band_id.their_title}
          </Text>
      </View>
    );
  },


});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  nav_stack: {
    width: 150,
    height: 150,
  },
});

AppRegistry.registerComponent('shakeconnect', () => shakeconnect);
